<div class="forside-hero">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>uvant</h1>
        <p>
          Lolland Falster byder på mange forskellige unikke produkter, udvid din horisont, prøv noget nyt, prøv noget uvant.
        </p>

        <div class="hero-btn-wrap">
          <a class="hero-btn hero-btn-1" href="#">Shop produkter</a>
          <a class="hero-btn hero-btn-2" href="#">Lær mere</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="grey-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Unikke produkter fra Lolland og Falster</h1>
      </div>

      <div class="col-md-6">
        <h2>Dette skal være en supplerende overskrift</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis non odit sordidos, vanos, leves, futtiles? Quamquam tu hanc copiosiorem etiam soles dicere. Si qua in iis corrigere voluit, deteriora fecit. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam.
          <br><br>
          Iam id ipsum absurdum, maximum malum neglegi. Duo Reges: constructio interrete. Ergo in gubernando nihil, in officio plurimum interest, quo in genere peccetur. Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
        </p>
      </div>

      <div class="col-md-6">
        <h2>Dette skal være en supplerende overskrift</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis non odit sordidos, vanos, leves, futtiles? Quamquam tu hanc copiosiorem etiam soles dicere. Si qua in iis corrigere voluit, deteriora fecit. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam.
          <br><br>
          Iam id ipsum absurdum, maximum malum neglegi. Duo Reges: constructio interrete. Ergo in gubernando nihil, in officio plurimum interest, quo in genere peccetur. Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
        </p>
      </div>
    </div>
  </div>
</div>


<div class="boxes">
  <div class="container-fluid">
    <div class="row no-gutter">
      <div class="col-md-6">
        <div class="box-top-left">
          <div class="box-content">
            <h1>opdag unikke produkter</h1>
            <hr class="box-hr">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis non odit sordidos, vanos, leves, futtiles? Quamquam tu hanc copiosiorem etiam soles dicere.
            </p>
            <br>
            <a href="#">Se nuværende udvalg</a>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="box-top-right">
        </div>
      </div>

      <div class="col-md-6 hidden-xs">
        <div class="box-bottom-left">
        </div>
      </div>

      <div class="col-md-6">
        <div class="box-bottom-right">
          <div class="box-content">
            <h1>Ordre sendt direkte til leverandør</h1>
            <hr class="box-hr">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis non odit sordidos, vanos, leves, futtiles? Quamquam tu hanc copiosiorem etiam soles dicere.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
